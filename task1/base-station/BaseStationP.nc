// $Id: BaseStationP.nc,v 1.10 2008/06/23 20:25:14 regehr Exp $

/*                                  tab:4
 * "Copyright (c) 2000-2005 The Regents of the University  of California.
 * All rights reserved.
 *
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose, without fee, and without written agreement is
 * hereby granted, provided that the above copyright notice, the following
 * two paragraphs and the author appear in all copies of this software.
 *
 * IN NO EVENT SHALL THE UNIVERSITY OF CALIFORNIA BE LIABLE TO ANY PARTY FOR
 * DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT
 * OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF THE UNIVERSITY OF
 * CALIFORNIA HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * THE UNIVERSITY OF CALIFORNIA SPECIFICALLY DISCLAIMS ANY WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS
 * ON AN "AS IS" BASIS, AND THE UNIVERSITY OF CALIFORNIA HAS NO OBLIGATION TO
 * PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS."
 *
 * Copyright (c) 2002-2005 Intel Corporation
 * All rights reserved.
 *
 * This file is distributed under the terms in the attached INTEL-LICENSE
 * file. If you do not find these files, copies can be found by writing to
 * Intel Research Berkeley, 2150 Shattuck Avenue, Suite 1300, Berkeley, CA,
 * 94704.  Attention:  Intel License Inquiry.
 */

/*
 * @author Phil Buonadonna
 * @author Gilman Tolle
 * @author David Gay
 * Revision:    $Id: BaseStationP.nc,v 1.10 2008/06/23 20:25:14 regehr Exp $
 */

/*
 * BaseStationP bridges packets between a serial channel and the radio.
 * Messages moving from serial to radio will be tagged with the group
 * ID compiled into the TOSBase, and messages moving from radio to
 * serial will be filtered by that same group id.
 */

#include "AM.h"
#include "Serial.h"
#include "msg.h"
#include "printf.h"

module BaseStationP {
    uses {
        interface Boot;

        //interface Timer<TMilli> as SerialTimer;
        interface Timer<TMilli> as RadioTimer;

        interface Packet as RadioPacket;
        interface AMPacket as RadioAMPacket;
        interface SplitControl as RadioControl;
        interface AMSend as RadioAMSend;
        interface Receive as RadioReceive;

        interface Packet as SerialPacket;
        interface AMPacket as SerialAMPacket;
        interface SplitControl as SerialControl;
        interface AMSend as SerialAMSend;
        interface Receive as SerialReceive;

        interface Leds;
    }
}

implementation
{
    enum {
        SERIAL_QUEUE_LEN = 12,
        RADIO_QUEUE_LEN = 12
    };

    uint16_t frequency = INIT_SAMPLING_FREQUENCY;

    message_t serialQueue[SERIAL_QUEUE_LEN];
    uint32_t serialBegin = 0;
    uint32_t serialEnd = 0;
    bool serialBusy = FALSE;

    message_t radioQueue[RADIO_QUEUE_LEN];
    uint32_t radioBegin = 0;
    uint32_t radioEnd = 0;
    bool radioBusy = FALSE;

    void forwardToSerial(void* msg_data);
    void forwardToRadio(void* msg_data);

    //Booted
    event void Boot.booted() {
        call RadioControl.start();
        call SerialControl.start();
        //call SerialTimer.startPeriodic(frequency/2);
        call RadioTimer.startPeriodic(frequency/2);
    }

    //Radio start done
    event void RadioControl.startDone(error_t err) {
        if (err!=SUCCESS)
            call RadioControl.start();
    }

    //Radio stop done
    event void RadioControl.stopDone(error_t err) {}

    //Serial start done
    event void SerialControl.startDone(error_t err) {
        if (err!=SUCCESS)
            call SerialControl.start();
    }

    //Serial stop done
    event void SerialControl.stopDone(error_t err) {}

    //Received data from radio
    event message_t* RadioReceive.receive(message_t* msg, void* payload, uint8_t len) {
        //RadioMsg only
        if (len==sizeof(RecordMsg)) {
            forwardToSerial(payload);
        }
        return msg;
    }

    //Received data from radio
    event message_t* SerialReceive.receive(message_t* msg, void* payload, uint8_t len) {
        //ControlMsg only
        if (len==sizeof(ControlMsg)) {
            forwardToRadio(payload);
        }
        return msg;
    }

    //Serial send data task
    task void serialSendTask() {
        message_t* packet;

        packet = serialQueue+serialEnd%SERIAL_QUEUE_LEN;
        //Try to send packet
        if (call SerialAMSend.send(AM_BROADCAST_ADDR, packet, sizeof(RecordMsg))==SUCCESS) {
            //Busy state
            serialBusy = TRUE;

            call Leds.led1Toggle();
        } else {
            //Update queue end
            serialEnd++;
            //Next send task
            if (serialBegin>serialEnd) {
                post serialSendTask();
            }

            call Leds.led0Toggle();
        }
    }

    //Forward data to serial
    void forwardToSerial(void* msg) {
        message_t* packet;
        void* payload;
        //Queue full
        if (serialEnd+SERIAL_QUEUE_LEN<=serialBegin)
            return;
        packet = serialQueue+serialBegin%SERIAL_QUEUE_LEN;
        payload = call SerialAMSend.getPayload(packet, sizeof(RecordMsg));
        //Copy data
        memcpy(payload, msg, sizeof(RecordMsg));

        //Start serial send task
        if (serialBegin==serialEnd) {
            post serialSendTask();
        }
        //Update queue begin
        serialBegin++;
    }

    //Forward data to radio
    void forwardToRadio(void* msg) {
        message_t* packet;
        void* payload;
        //Queue full
        if (radioEnd+RADIO_QUEUE_LEN<=radioBegin)
            return;
        packet = radioQueue+radioBegin%RADIO_QUEUE_LEN;
        payload = call RadioAMSend.getPayload(packet, sizeof(ControlMsg));
        //Copy data
        memcpy(payload, msg, sizeof(ControlMsg));
        //Update queue begin
        radioBegin++;
    }

    //Serial send done
    event void SerialAMSend.sendDone(message_t* msg, error_t err) {
        serialBusy = FALSE;
        serialEnd++;

        //Next send task
        if (serialBegin>serialEnd) {
            post serialSendTask();
        }

        if (err==SUCCESS) {
            call Leds.led1Toggle();
        } else {
            call Leds.led0Toggle();
        }
    }

    //Radio send done
    event void RadioAMSend.sendDone(message_t* msg, error_t err) {
        radioBusy = FALSE;
        radioEnd++;

        if (err==SUCCESS) {
            call Leds.led2Toggle();
        } else {
            call Leds.led0Toggle();
        }
    }

    event void RadioTimer.fired() {
        message_t* packet;

        //Nothing to send
        if (radioBegin==radioEnd)
            return;
        //Still busy; do not send
        if (radioBusy)
            return;

        packet = radioQueue+radioEnd%RADIO_QUEUE_LEN;
        if (call RadioAMSend.send(AM_BROADCAST_ADDR, packet, sizeof(ControlMsg))==SUCCESS) {
            radioBusy = TRUE;
            call Leds.led1Toggle();
        } else {
            radioEnd++;
            call Leds.led0Toggle();
        }
    }
}
