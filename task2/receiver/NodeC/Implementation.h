//Qsort-like median procress
uint16_t partition(uint16_t* root, uint16_t low, uint16_t high) {
    uint16_t part = root[low];
    while (low<high) {
        while ((low<high)&&(root[high]>part)) {
            high--;
        }
        root[low] = root[high];
        while ((low<high)&&(root[low]<=part)) {
            low++;
        }
        root[high] = root[low];
    }
    root[low] = part;
    return low;
}

uint16_t select(uint16_t *root, uint16_t length, uint16_t pos) {
    uint16_t low = 0, high = length - 1;
    while (high > low) {
        uint16_t j = partition(root, low, high);
        if (j == pos) {
            return root[pos];
        } else if (j > pos) {
            high = j - 1;
        } else if (j < pos) {
            low = j + 1;
        }
    }
    return root[pos];
}

uint16_t medium(uint16_t *root1, uint16_t length) {
    uint16_t x, y;
    uint16_t i;

    root1[length] = 0;
    x = select(root1, length+1, length/2);
    for (i = 0; i < length; i++) {
        if (root1[i] == 0) {
            root1[i] = 0xffff;
            break;
        }
    }
    y = select(root1, length+1, length/2);

    return (x+y)/2;
}

//Calculation task
task void calcTask() {
    partResult.type = PARTIAL_RESULT_MSG;
    partResult.nodeId = TOS_NODE_ID;
    partResult.results[0] = medium(integers, N_INTEGERS);

    DEBUG_PRINT("[NodeC calcTask] median: %lu\n", partResult.results[0]);

    //Reset state
    partResultState = 0;
    //Start timeout timer
    call PartialResultTimeoutTimer.startOneShot(TIMEOUT);
    //Send data
    sendData((DataMsg*)(&partResult));
}

//Self query completed hook (Nothing)
void selfQueryCompleted() {}

//All queries completed hook
void allQueriesCompleted() {
    post calcTask();
}

//Data message dispatch table
BEGIN_DATA_MSG_DISPATCH
    DATA_MSG_MAP(QUERY_MSG, queryHandler)
    DATA_MSG_MAP(QUERY_RESP_MSG, queryRespHandler)
    DATA_MSG_MAP(PARTIAL_RESULT_ACK_MSG, partialResultAckHandler)
END_DATA_MSG_DISPATCH
