//Senders
components new AMSenderC(AM_RESULTMSG) as ResultMsgSender;
//Receivers
components new AMReceiverC(AM_RESULTACKMSG) as ResultAckMsgReceiver;
//Timers
components new TimerMilliC() as ResultTimeoutTimer;

//Senders
SenseC.ResultMsgSender -> ResultMsgSender;
//Receivers
SenseC.ResultAckMsgReceiver -> ResultAckMsgReceiver;
//Timers
SenseC.ResultTimeoutTimer -> ResultTimeoutTimer;
