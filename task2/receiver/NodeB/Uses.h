//Senders
interface AMSend as ResultMsgSender;
//Receivers
interface Receive as ResultAckMsgReceiver;
//Timers
interface Timer<TMilli> as ResultTimeoutTimer;
