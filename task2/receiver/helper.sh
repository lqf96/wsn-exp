#! /bin/sh
set -e

DEVICE_AMOUNT=3
DEVICE_ID=0
GROUP_ID=11

# Clean
make clean
# Check device existance and install program
while [ ! "${DEVICE_ID}" -ge "${DEVICE_AMOUNT}" ]; do
    if [ ! -c "/dev/ttyUSB${DEVICE_ID}" ]; then
        echo "Device ${DEVICE_ID} (/dev/ttyUSB${DEVICE_ID}) does not exist!" >&2
        exit 1
    fi
    # Install program
    INTERNAL_ID=`expr ${DEVICE_ID} + 1`
    NODE_ID=`echo "(${GROUP_ID}-1)*3+${INTERNAL_ID}" | bc`
    make telosb install,${NODE_ID} bsl,/dev/ttyUSB${DEVICE_ID} T2_ID=${INTERNAL_ID} T2_GROUP=${GROUP_ID}
    # Increase device ID
    DEVICE_ID=`expr ${DEVICE_ID} + 1`
done
